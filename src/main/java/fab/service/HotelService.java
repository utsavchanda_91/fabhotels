package fab.service;

import fab.entity.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fab.repository.HotelRepository;

/**
 * Created by utsav on 24/8/16.
 */
@Service
public class HotelService {

    @Autowired
    private HotelRepository hotelRepository;

    public Hotel fetchById(Long id){
        return hotelRepository.findOne(id);
    }

    public Hotel create(Hotel hotel){
        return hotelRepository.save(hotel);
    }

    public Hotel update(Hotel hotel){
        return hotelRepository.save(hotel);
    }
}
