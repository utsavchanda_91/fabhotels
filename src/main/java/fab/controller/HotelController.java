package fab.controller;

import fab.entity.Hotel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import fab.service.HotelService;

/**
 * Created by utsav on 24/8/16.
 */
@RestController
@RequestMapping("/hotels")
public class HotelController {

    @Autowired
    private HotelService hotelService;

    public Hotel fetchById(@PathVariable Long id){
        return hotelService.fetchById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Hotel create(@RequestBody Hotel hotel){
        return hotelService.create(hotel);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Hotel update(@RequestBody Hotel hotel){
        return hotelService.update(hotel);
    }


}
