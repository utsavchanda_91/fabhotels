package fab.entity.enums;

/**
 * Created by utsav on 24/8/16.
 */
public enum Attribute {

    RESTAURANT("Restaurant"),
    BAKERY_SHOP("BakeryShop"),
    TRAVEL_DESK("TravelDesk")
    ;

    Attribute(String val) {
    }
}
