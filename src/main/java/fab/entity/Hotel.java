package fab.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by utsav on 24/8/16.
 */
@Entity
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private Location location;

    @Column
    private Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<PointOfSale> getPointOfSales() {
        return pointOfSales;
    }

    public void setPointOfSales(Set<PointOfSale> pointOfSales) {
        this.pointOfSales = pointOfSales;
    }
}
