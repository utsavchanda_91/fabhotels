package fab.repository;

import fab.entity.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by utsav on 24/8/16.
 */
public interface HotelRepository extends JpaRepository<Hotel, Long> {

}
